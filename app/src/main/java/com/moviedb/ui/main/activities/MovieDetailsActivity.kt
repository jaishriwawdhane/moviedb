package com.moviedb.ui.main.activities

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.webkit.WebChromeClient
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.moviedb.R
import com.moviedb.ui.main.data.credits.Cast
import com.moviedb.ui.main.data.credits.Crew
import com.moviedb.ui.main.data.resposnse.Credits
import com.moviedb.ui.main.data.resposnse.MovieDetailsResponse
import com.moviedb.ui.main.data.similar.Result
import com.moviedb.ui.main.data.similar.SimilarMoviesResponse
import com.moviedb.ui.main.data.videos.Videos
import com.moviedb.ui.main.network.NetworkService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_movie_details.*
import kotlinx.android.synthetic.main.item_caste.*
import kotlinx.android.synthetic.main.item_movie.view.*


class MovieDetailsActivity : AppCompatActivity(), Player.EventListener {
    private lateinit var videoPlayer: SimpleExoPlayer
    private var playbackPosition: Long = 0
    private val YOUTUBE_KEY: String = "AIzaSyBc6JNsqGxVBKMdXXtMqh-kQlcbe4he-IE"

    private val dataSourceFactory: DataSource.Factory by lazy {
        DefaultDataSourceFactory(this, "exoplayer-sample")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        val intent: Intent = getIntent()
        val responseDTO: MovieDetailsResponse? =
            intent.getParcelableExtra<MovieDetailsResponse>("Details")
        val releaseDate = responseDTO?.release_date
        val title = responseDTO?.title
        val videoUrl = responseDTO?.homepage
        val movie_id = intent.getStringExtra("movie_id")

        txt_movie_name.text = "Title:- $title"
        txtReleaseDate.text = "Release Date:- $releaseDate"

        imgVideo.getSettings().setJavaScriptEnabled(true);
        imgVideo.setWebChromeClient(object : WebChromeClient() {})

        val casteAndCrew: Credits? = intent.getParcelableExtra<Credits>("Credit")
        val videos: Videos? = intent.getParcelableExtra<Videos>("Videos")
        val similarMovies: SimilarMoviesResponse? = intent.getParcelableExtra<SimilarMoviesResponse>("SimilarMovies")
        val key = videos?.results?.get(0)?.key
        val youtubeVideo =
            "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/$key\" frameborder=\"0\" allowfullscreen></iframe>"

        youtubeVideo?.let {
            if (!youtubeVideo.equals("")) {
                imgVideo.loadData(it, "text/html", "utf-8")
            };
        }

        setCaste(casteAndCrew?.cast)
        setCrew(casteAndCrew?.crew)
        setSimilarMovies(similarMovies?.results)
    }

    private fun setSimilarMovies(results: List<Result>?) {
        llSimilarMovies.removeAllViews()
        for (i in 0..(results!!.size-1 )){
            val similarResults = results?.get(i)
            val viewSimilar: View = LayoutInflater.from(this).inflate(
                R.layout.item_caste,
                llSimilarMovies, false)
            val txtOriginalName = viewSimilar.findViewById<TextView>(R.id.txtOriginalName)
            txtOriginalName.text = similarResults.title
            val txtPopularity = viewSimilar.findViewById<TextView>(R.id.txtPopularity)
            txtPopularity.text = similarResults.releaseDate.toString()
            llSimilarMovies.addView(viewSimilar)


            val imgCaste  = viewSimilar.findViewById<ImageView>(R.id.imgCaste)
            if (!results?.get(i).backdropPath.isNullOrEmpty()){
                val networkService = NetworkService.getService()
                networkService.getImages(results?.get(i).id!!.toInt(), "en-US", results.get(i).backdropPath.toString())
                    .subscribeOn(Schedulers.single())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                            v ->
                        //Log.d("Images: ", v.backdrops.get(0))
                        if (!v.backdrops.isEmpty()) {
                            Glide.with(imgCaste.context)
                                .load(v.backdrops.get(0))
                                .apply(
                                    RequestOptions().placeholder(R.drawable.ic_launcher_background)
                                        .error(R.drawable.ic_launcher_background))
                                .into(imgCaste)
                        }else{
                            imgCaste.setImageResource(R.drawable.ic_launcher_background)
                        }

                    },
                        { e -> println("Error: $e") }
                    )
            }



        }

    }

    private fun setCrew(crew: List<Crew>?) {
        llCrew.removeAllViews()
        for (i in 0..(crew!!.size-1 )){
            val movieCrew = crew?.get(i)
            val viewCrew: View = LayoutInflater.from(this).inflate(
                R.layout.item_caste,
                llCrew, false)
            val txtOriginalName = viewCrew.findViewById<TextView>(R.id.txtOriginalName)
            txtOriginalName.text = movieCrew.originalName
            val txtPopularity = viewCrew.findViewById<TextView>(R.id.txtPopularity)
            txtPopularity.text = movieCrew.knownForDepartment.toString()
            val imgCaste  = viewCrew.findViewById<ImageView>(R.id.imgCaste)
            if (!movieCrew.profilePath.isNullOrEmpty()){
                val networkService = NetworkService.getService()
                networkService.getImages(movieCrew.id!!.toInt(), "en-US", movieCrew.profilePath.toString())
                    .subscribeOn(Schedulers.single())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                            v ->
                        //Log.d("Images: ", v.backdrops.get(0))
                        if (!v.backdrops.isEmpty()) {
                            Glide.with(imgCaste.context)
                                .load(v.backdrops.get(0))
                                .apply(
                                    RequestOptions().placeholder(R.drawable.ic_launcher_background)
                                        .error(R.drawable.ic_launcher_background))
                                .into(imgCaste)
                        }else{
                            imgCaste.setImageResource(R.drawable.ic_launcher_background)
                        }

                    },
                        { e -> println("Error: $e") }
                    )
            }

            llCrew.addView(viewCrew)


        }
    }

    private fun setCaste(cast: List<Cast>?) {
        llCaste.removeAllViews()
        for (i in 0..(cast!!.size-1 )){
            val movieCast = cast?.get(i)
            val viewCaste: View = LayoutInflater.from(this).inflate(
                    R.layout.item_caste,
                    llCaste, false)
            val txtOriginalName = viewCaste.findViewById<TextView>(R.id.txtOriginalName)
            txtOriginalName.text = movieCast.originalName
            val txtPopularity = viewCaste.findViewById<TextView>(R.id.txtPopularity)
            txtPopularity.text = movieCast.popularity.toString()
            val imgCaste  = viewCaste.findViewById<ImageView>(R.id.imgCaste)
            if (!movieCast.profilePath.isNullOrEmpty()){
                val networkService = NetworkService.getService()
                networkService.getImages(movieCast.id!!.toInt(), "en-US", movieCast.profilePath.toString())
                    .subscribeOn(Schedulers.single())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                            v ->
                        //Log.d("Images: ", v.backdrops.get(0))
                        if (!v.backdrops.isEmpty()) {
                            Glide.with(imgCaste.context)
                                .load(v.backdrops.get(0))
                                .apply(
                                    RequestOptions().placeholder(R.drawable.ic_launcher_background)
                                        .error(R.drawable.ic_launcher_background))
                                .into(imgCaste)
                        }else{
                            imgCaste.setImageResource(R.drawable.ic_launcher_background)
                        }

                    },
                        { e -> println("Error: $e") }
                    )
            }


            llCaste.addView(viewCaste)




        }

    }


    /*private fun buildMediaSource( youtubeVideo:String): MediaSource? {
        val dataSourceFactory = DefaultDataSourceFactory(this, "sample")
        return ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(youtubeVideo))
    }

    override fun onResume() {
        super.onResume()
//        videoPlayer?.playWhenReady = true
    }

    override fun onStop() {
        super.onStop()
        //videoPlayer?.playWhenReady = false
        if (isFinishing) {
           // releasePlayer()
        }
    }

    private fun releasePlayer() {
        videoPlayer?.release()
    }*/


}