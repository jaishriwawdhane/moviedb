package com.moviedb.ui.main.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.moviedb.R
import com.moviedb.ui.main.adaptor.MovieListAdaptor
import com.moviedb.ui.main.data.State
import com.moviedb.ui.main.data.model.ResultsDTO
import com.moviedb.ui.main.listeners.PlayCLickListener
import com.moviedb.ui.main.network.NetworkService
import com.moviedb.ui.main.viewmodel.MoviesViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_movies_list.*

class MoviesListActivity : AppCompatActivity(), PlayCLickListener {
    private lateinit var viewModel: MoviesViewModel
    private lateinit var movieListAdaptor: MovieListAdaptor
    private lateinit var playCLickListener: PlayCLickListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies_list)
        viewModel = ViewModelProvider(this).get(MoviesViewModel::class.java)
        initAdapter()
        initState()
    }


    private fun initAdapter() {
        playCLickListener = MainActivity@ this
        movieListAdaptor = MovieListAdaptor({ viewModel.retry() }, playCLickListener)
        recycler_view.adapter = movieListAdaptor
        viewModel.movieList.observe(this,
            Observer {
                movieListAdaptor.submitList(it)
            })
    }

    private fun initState() {
        txt_error.setOnClickListener { viewModel.retry() }
        viewModel.getState().observe(this, Observer { state ->
            progress_bar.visibility =
                if (viewModel.listIsEmpty() && state == State.LOADING) View.VISIBLE else View.GONE
            txt_error.visibility =
                if (viewModel.listIsEmpty() && state == State.ERROR) View.VISIBLE else View.GONE
            if (!viewModel.listIsEmpty()) {
                movieListAdaptor.setState(state ?: State.DONE)
            }
        })
    }

    override fun playNow(movie: ResultsDTO) {
        val intent = Intent(MoviesListActivity@ this, MovieDetailsActivity::class.java)
        //   Toast.makeText(MoviesListActivity@this, "Moview Name:${movie.title} al ${movie.id}", Toast.LENGTH_LONG).show()
        val networkService = NetworkService.getService()
        networkService.getMoviewDetails(movie.id, "en-US")
                .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ v ->

                intent.apply {
                    putExtra("Details", v)
                    putExtra("movie_id", movie.id)
                }
               // startActivity(intent)
            },
                { e -> println("Error: $e") }
            )

        networkService.getMovieCredits(movie.id, "en-US")
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ v ->

                    intent.apply {
                        putExtra("Credit", v)
                    }
                    //startActivity(intent)
                },
                        { e -> println("Error: $e") }
                )

        networkService.getMovieVideos(movie.id, "en-US")
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ v ->

                    intent.apply {
                        putExtra("Videos", v)
                    }
                 //   startActivity(intent)
                },
                        { e -> println("Error: $e") }
                )
        networkService.getSimilarMovies(movie.id, "en-US", 1)
            .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ v ->

                intent.apply {
                    putExtra("SimilarMovies", v)
                }
                startActivity(intent)
            },
                { e -> println("Error: $e") }
            )
    }

}