package com.moviedb.ui.main.adaptor

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.moviedb.R
import com.moviedb.ui.main.data.State
import com.moviedb.ui.main.data.model.ResultsDTO
import com.moviedb.ui.main.listeners.PlayCLickListener
import com.moviedb.ui.main.network.NetworkService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_movie.view.*


class MovieListAdaptor(
    private val retry: () -> Unit,
    private val playCLickListener: PlayCLickListener
) :
    PagedListAdapter<ResultsDTO, RecyclerView.ViewHolder>(MoviesDiffCallback) {
    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2
    private var state = State.LOADING

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == DATA_VIEW_TYPE) MoviesViewHolder.create(parent)
        else ListFooterViewHolder.create(retry, parent)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == DATA_VIEW_TYPE)
            (holder as MoviesViewHolder).bind(getItem(position), playCLickListener)
        else (holder as ListFooterViewHolder).bind(state)
    }

    class MoviesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(
            movie: ResultsDTO?,
            playCLickListener: PlayCLickListener
        ) {
            if (movie != null) {
                itemView.txt_movie_name.text = movie.title
                itemView.txt_releaseDate.text = movie.releaseDate
                itemView.txt_rating.text = movie.voteAverage.toString()



                //https://api.themoviedb.org/3/movie/464052/images?api_key=e21cd7b9b607b40be0c1a719282e2c3a&language=en-US&include_image_language=%2Fn9KlvCOBFDmSyw3BgNrkUkxMFva.jpg


                if (!movie.backdropPath.isNullOrEmpty()){
                    val networkService = NetworkService.getService()
                    networkService.getImages(movie.id, "en-US", movie.backdropPath)
                        .subscribeOn(Schedulers.single())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                             v ->
                            //Log.d("Images: ", v.backdrops.get(0))
                            if (!v.backdrops.isEmpty()) {
                                Glide.with(itemView.context)
                                        .load(v.backdrops.get(0))
                                        .apply(
                                                RequestOptions().placeholder(R.drawable.ic_launcher_background)
                                                        .error(R.drawable.ic_launcher_background))
                                        .into(itemView.img_banner)
                            }else{
                                Glide.with(itemView.context)
                                        .load(movie.backdropPath)
                                        .apply(
                                                RequestOptions().placeholder(R.drawable.ic_launcher_background)
                                                        .error(R.drawable.ic_launcher_background))
                                        .into(itemView.img_banner)
                            }

                        },
                            { e -> println("Error: $e") }
                        )
                }




                itemView.btnPlayNow.setTag(R.id.moviedetails, movie.id)
                itemView.btnPlayNow.setOnClickListener({
                    playCLickListener.playNow(movie)
                })






            }
        }

        companion object {
            fun create(
                parent: ViewGroup
            ): MoviesViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_movie, parent, false)
                return MoviesViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }

    companion object {
        val MoviesDiffCallback = object : DiffUtil.ItemCallback<ResultsDTO>() {
            override fun areItemsTheSame(oldItem: ResultsDTO, newItem: ResultsDTO): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areContentsTheSame(oldItem: ResultsDTO, newItem: ResultsDTO): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }

    class MovieDetailsClickListener:View.OnClickListener{
        override fun onClick(v: View?) {
            TODO("Not yet implemented")
        }

    }
}
