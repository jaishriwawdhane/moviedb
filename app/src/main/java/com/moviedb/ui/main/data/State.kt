package com.moviedb.ui.main.data

enum class State {
    DONE, LOADING, ERROR
}