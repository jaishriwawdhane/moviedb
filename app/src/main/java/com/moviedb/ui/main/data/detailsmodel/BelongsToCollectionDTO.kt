package com.moviedb.ui.main.data.detailsmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
@Parcelize
data class BelongsToCollectionDTO(
	val id: Int? = null,
	val name: String? = null,
	val posterPath: String? = null,
	val backdropPath: String? = null
):Parcelable