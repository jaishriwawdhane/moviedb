package com.moviedb.ui.main.data.detailsmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
@Parcelize
data class GenresDTO(
	val id: Int? = null,
	val name: String? = null
):Parcelable