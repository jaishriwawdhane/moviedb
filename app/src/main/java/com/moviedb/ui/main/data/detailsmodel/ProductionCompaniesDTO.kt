package com.moviedb.ui.main.data.detailsmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
@Parcelize
data class ProductionCompaniesDTO(
	val id: Int? = null,
	val logoPath: String? = null,
	val name: String? = null,
	val originCountry: String? = null
):Parcelable