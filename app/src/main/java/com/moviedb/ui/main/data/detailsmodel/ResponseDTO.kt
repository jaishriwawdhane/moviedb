package com.moviedb.ui.main.data.detailsmodel

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseDTO(
	val adult: Boolean? = null,
	val backdropPath: String? = null,
	val belongsToCollection: BelongsToCollectionDTO? = null,
	val budget: Int? = null,
	val genres: List<GenresDTO?>? = null,
	val homepage: String? = null,
	val id: Int? = null,
	val imdbId: String? = null,
	val originalLanguage: String? = null,
	val originalTitle: String? = null,
	val overview: String? = null,
	val popularity: Float? = null,
	val posterPath: String? = null,
	val productionCompanies: List<ProductionCompaniesDTO?>? = null,
	val productionCountries: List<ProductionCountriesDTO?>? = null,
	@SerializedName("release_date")
	val releaseDate: String? = null,
	val revenue: Int? = null,
	val runtime: Int? = null,
	val spokenLanguages: List<SpokenLanguagesDTO?>? = null,
	val status: String? = null,
	val tagline: String? = null,
	val title: String? = null,
	val video: Boolean? = null,
	val voteAverage: Float? = null,
	val voteCount: Int? = null
):Parcelable