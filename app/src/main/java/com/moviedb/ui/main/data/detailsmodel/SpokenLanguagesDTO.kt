package com.moviedb.ui.main.data.detailsmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
@Parcelize
data class SpokenLanguagesDTO(
	val englishName: String? = null,
	val iso6391: String? = null,
	val name: String? = null
):Parcelable