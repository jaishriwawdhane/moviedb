package com.moviedb.ui.main.data.imagedb

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageDBResponse(val id: Long, val backdrops: List<String>, val posters: List<String>) :
    Parcelable