package com.moviedb.ui.main.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class DatesDTO(
	val maximum: String? = null,
	val minimum: String? = null
):Parcelable