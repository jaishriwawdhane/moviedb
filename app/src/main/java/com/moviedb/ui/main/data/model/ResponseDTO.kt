package com.moviedb.ui.main.data.model

data class ResponseDTO(
	val dates: DatesDTO? = null,
	val page: Int? = null,
	val results: List<ResultsDTO?>? = null,
	val totalPages: Int? = null,
	val totalResults: Int? = null
)