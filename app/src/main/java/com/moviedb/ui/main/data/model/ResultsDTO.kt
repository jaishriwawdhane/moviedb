package com.moviedb.ui.main.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResultsDTO(
	val adult: Boolean,
	@SerializedName("backdrop_path")
	val backdropPath: String,
	val genreIds: List<Int?>,
	val id: Int,
	val originalLanguage: String,
	val originalTitle: String,
	val overview: String,

	val popularity: Float,
	@SerializedName("poster_path")
	val posterPath: String,
	@SerializedName("release_date")
	val releaseDate: String,
	val title: String,
	@SerializedName("video")
	val video: Boolean,
	@SerializedName("vote_average")
	val voteAverage: Float,
	@SerializedName("vote_count")
	val voteCount: Int
):Parcelable