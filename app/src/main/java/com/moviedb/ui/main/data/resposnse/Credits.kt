package com.moviedb.ui.main.data.resposnse

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.moviedb.ui.main.data.credits.Cast
import com.moviedb.ui.main.data.credits.Crew
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Credits(
        @SerializedName("cast")
        var cast: List<Cast>? = null,

        @SerializedName("crew")
        var crew: List<Crew>? = null,

        @SerializedName("id")
        var id: Long? = null

) : Parcelable