package com.moviedb.ui.main.data.resposnse

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.moviedb.ui.main.data.detailsmodel.BelongsToCollectionDTO
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieDetailsResponse(
        @SerializedName("belongs_to_collection")
        val belongsToCollectionDTO: BelongsToCollectionDTO?,
        @SerializedName("homepage")
        val homepage: String,
        @SerializedName("release_date")
        val release_date:String,
        @SerializedName("title")
        val title:String

):Parcelable