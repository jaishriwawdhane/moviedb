package com.moviedb.ui.main.data.resposnse

import com.google.gson.annotations.SerializedName
import com.moviedb.ui.main.data.model.DatesDTO
import com.moviedb.ui.main.data.model.ResultsDTO

data class MovieListResponse(
    @SerializedName("dates")
    val datesDTO: DatesDTO,

    @SerializedName("page")
    val page: Int = 0,

    @SerializedName("results")
    val result: List<ResultsDTO>

)


