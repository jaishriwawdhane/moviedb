package com.moviedb.ui.main.data.videos

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Result (
    @Expose
    var id: String? = null,

    @SerializedName("iso_3166_1")
    var iso31661: String? = null,

    @SerializedName("iso_639_1")
    var iso6391: String? = null,

    @Expose
    var key: String? = null,

    @Expose
    var name: String? = null,

    @Expose
    var site: String? = null,

    @Expose
    var size: Long? = null,

    @Expose
    var type: String? = null

):Parcelable