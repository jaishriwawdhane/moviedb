package com.moviedb.ui.main.data.videos

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.android.parcel.Parcelize

@Parcelize
class Videos(
        @Expose
        var id: Long? = null,

        @Expose
        var results: List<Result>? = null

        ) : Parcelable