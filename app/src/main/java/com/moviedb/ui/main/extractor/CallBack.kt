package com.moviedb.ui.main.extractor

public interface CallBack {
    interface playerCallBack {
        fun onItemClickOnItem(albumId: Int?)
        fun onPlayingEnd()
    }
}