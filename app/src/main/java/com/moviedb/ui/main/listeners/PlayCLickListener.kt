package com.moviedb.ui.main.listeners

import com.moviedb.ui.main.data.model.ResultsDTO

interface PlayCLickListener {
    fun playNow(movie: ResultsDTO)
}