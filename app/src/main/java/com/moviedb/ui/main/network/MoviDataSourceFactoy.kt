package com.moviedb.ui.main.network

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.moviedb.ui.main.data.model.ResultsDTO
import io.reactivex.disposables.CompositeDisposable

class MoviDataSourceFactoy(private val compositeDisposable: CompositeDisposable,
                           private val networkService: NetworkService)
    : DataSource.Factory<Int, ResultsDTO>() {

    val movieDataSourceLiveData = MutableLiveData<MovieDataSource>()

    override fun create(): DataSource<Int, ResultsDTO> {
        val movieDataSource = MovieDataSource(networkService, compositeDisposable)
        movieDataSourceLiveData.postValue(movieDataSource)
        return movieDataSource
    }
}