package com.moviedb.ui.main.network

import androidx.lifecycle.MutableLiveData
import com.moviedb.ui.main.data.model.ResultsDTO
import io.reactivex.disposables.CompositeDisposable
import androidx.paging.PageKeyedDataSource
import com.moviedb.ui.main.data.State
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers


class MovieDataSource(
    private val networkService: NetworkService,
    private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Int, ResultsDTO>() {
    var state: MutableLiveData<State> = MutableLiveData()
    private var retryCompletable: Completable? = null
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, ResultsDTO>
    ) {
        updateState(State.LOADING)
        compositeDisposable.add(networkService.getPlayList("en-US", 1).subscribe(
            { response ->
                updateState(State.DONE)
                callback.onResult(response.result,
                    null,
                    2
                )
            },
            {
                updateState(State.ERROR)
                setRetry(Action { loadInitial(params, callback) })
            }
        ))
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, ResultsDTO>) {
        updateState(State.LOADING)
        compositeDisposable.add(
            networkService.getPlayList("en-US", params.key)
                .subscribe(
                    { response ->
                        updateState(State.DONE)
                        callback.onResult(response.result,
                            params.key + 1
                        )
                    },
                    {
                        updateState(State.ERROR)
                        setRetry(Action { loadAfter(params, callback) })
                    }
                )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, ResultsDTO>) {
        TODO("Not yet implemented")
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }

    fun retry() {
        if (retryCompletable != null) {
            compositeDisposable.add(retryCompletable!!
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe())
        }
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }

}