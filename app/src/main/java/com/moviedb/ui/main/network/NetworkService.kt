package com.moviedb.ui.main.network

import com.moviedb.ui.main.data.imagedb.ImageDBResponse
import com.moviedb.ui.main.data.resposnse.Credits
import com.moviedb.ui.main.data.resposnse.MovieDetailsResponse
import com.moviedb.ui.main.data.resposnse.MovieListResponse
import com.moviedb.ui.main.data.similar.SimilarMoviesResponse
import com.moviedb.ui.main.data.videos.Videos
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NetworkService {
///3/movie/now_playing?api_key=e21cd7b9b607b40be0c1a719282e2c3a&language=en-US&page=1

    @GET("/3/movie/now_playing?api_key=e21cd7b9b607b40be0c1a719282e2c3a")
    fun getPlayList(
        @Query("language") language: String,
        @Query("page") page: Int
    ): Single<MovieListResponse>
//https://api.themoviedb.org/3/movie/464052?api_key=e21cd7b9b607b40be0c1a719282e2c3a&language=en-US
    @GET("/3/movie/{movie_id}?api_key=e21cd7b9b607b40be0c1a719282e2c3a")
    fun getMoviewDetails(
        @Path("movie_id") movieId: Int,
        @Query("language") language: String
    ): Single<MovieDetailsResponse>

    //https://api.themoviedb.org/3/movie/464052/credits?api_key=e21cd7b9b607b40be0c1a719282e2c3a&language=en-US
    @GET("/3/movie/{movie_id}/credits?api_key=e21cd7b9b607b40be0c1a719282e2c3a")
    fun getMovieCredits(
            @Path("movie_id") movieId: Int,
            @Query("language") language: String
    ): Single<Credits>

    //https://api.themoviedb.org/3/movie/464052/credits?api_key=e21cd7b9b607b40be0c1a719282e2c3a&language=en-US
    @GET("/3/movie/{movie_id}/videos?api_key=e21cd7b9b607b40be0c1a719282e2c3a")
    fun getMovieVideos(
            @Path("movie_id") movieId: Int,
            @Query("language") language: String
    ): Single<Videos>

    //https://api.themoviedb.org/3/movie/464052/similar?api_key=e21cd7b9b607b40be0c1a719282e2c3a&language=en-US&page=1
    @GET("/3/movie/{movie_id}/similar?api_key=e21cd7b9b607b40be0c1a719282e2c3a")
    fun getSimilarMovies(
        @Path("movie_id") movieId: Int,
        @Query("language") language: String,
        @Query("page") page: Int
    ): Single<SimilarMoviesResponse>

    //https://api.themoviedb.org/3/movie/550/images?api_key=e21cd7b9b607b40be0c1a719282e2c3a&language=en-US&include_image_language=/8kNruSfhk5IoE4eZOc4UpvDn6tq.jpg
    @GET("/3/movie/{movie_id}/images?api_key=e21cd7b9b607b40be0c1a719282e2c3a")
    fun getImages(
        @Path("movie_id") movieId: Int,
        @Query("language") language: String,
        @Query("include_image_language") page: String
    ): Single<ImageDBResponse>


    companion object {
       public fun getService(): NetworkService {
            val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.themoviedb.org")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(NetworkService::class.java)
        }
    }

}