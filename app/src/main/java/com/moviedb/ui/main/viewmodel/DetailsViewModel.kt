package com.moviedb.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moviedb.ui.main.data.State
import com.moviedb.ui.main.data.model.ResponseDTO
import com.moviedb.ui.main.network.NetworkService
import io.reactivex.disposables.CompositeDisposable

class DetailsViewModel:ViewModel() {

    private val networkService = NetworkService.getService()
   // var detailMovieInfo : LiveData<ResponseDTO>
    private val compositeDisposable = CompositeDisposable()
    init {
        var state: MutableLiveData<State> = MutableLiveData()



    }

    public fun getDetails() {
        updateState(State.LOADING)
        /*compositeDisposable.add(networkService.getMoviewDetails("en-US", movieId).subscribe(
            { response ->
                updateState(State.DONE)
            }
        ))*/
    }

    private fun updateState(state: State) {
   //     this.state.postValue(state)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}