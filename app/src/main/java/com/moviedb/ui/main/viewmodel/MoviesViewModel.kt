package com.moviedb.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.moviedb.ui.main.data.State
import com.moviedb.ui.main.data.model.ResultsDTO
import com.moviedb.ui.main.network.MoviDataSourceFactoy
import com.moviedb.ui.main.network.MovieDataSource
import com.moviedb.ui.main.network.NetworkService
import io.reactivex.disposables.CompositeDisposable

class MoviesViewModel: ViewModel() {
    private val networkService = NetworkService.getService()
    var movieList : LiveData<PagedList<ResultsDTO>>
    private val compositeDisposable = CompositeDisposable()
    private val pageSize = 1
    private val moviDataSourceFactoy: MoviDataSourceFactoy
    init {
        moviDataSourceFactoy = MoviDataSourceFactoy(compositeDisposable, networkService)
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize )
            .setEnablePlaceholders(false)
            .build()
        movieList = LivePagedListBuilder(moviDataSourceFactoy, config).build()
    }

    fun getState(): LiveData<State> = Transformations.switchMap(
        moviDataSourceFactoy.movieDataSourceLiveData,
        MovieDataSource::state
    )

    fun retry() {
        moviDataSourceFactoy.movieDataSourceLiveData.value?.retry()
    }

    fun listIsEmpty(): Boolean {
        return movieList.value?.isEmpty() ?: true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}